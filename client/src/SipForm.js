import React, { useState } from 'react';
import axios from 'axios';
//

const SipForm = () => {
  const [uri, setUri] = useState('');
  const [password, setPassword] = useState('');
  const [socket, setSocket] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false); // State variable for button disabled state

  const handleSubmit = (e) => {
    e.preventDefault();

    const config = { uri, password, socket };

    let timeout; // Variable to store the timeout

    setIsSubmitting(true); // Disable the submit button

    axios.post('/api', config)
      .then((response) => {
        clearTimeout(timeout); // Clear the timeout if a response is received
        console.log('Data sent successfully:', response.data);
        alert(`Data sent successfully: ${response.data}`);
      })
      .catch((error) => {
        clearTimeout(timeout); // Clear the timeout if an error response is received
        console.error('Error sending data:', error);
        alert(`Error sending data: ${error}`);
      })
      .finally(() => {
        setIsSubmitting(false); // Enable the submit button
      });

    // Set a timeout of 10 seconds
    timeout = setTimeout(() => {
      console.log('Timeout: No response from server');
      alert('Failed to create SIP Connection');
      setIsSubmitting(false); // Enable the submit button
    }, 10000);
  };

  return (
    <form onSubmit={handleSubmit}>
      <h2 style={{ width: "100%", textAlign: "center", marginBottom: "20px", color: "blue" }}>SIP Configuration</h2>
      <div className="mb-3">
        <label style={{ width: "100%", textAlign: "center" }} htmlFor="uri" className="form-label">URI:</label>
        <input type="text" className="form-control" id="uri" value={uri} onChange={(e) => setUri(e.target.value)} />
      </div>
      <div className="mb-3">
        <label style={{ width: "100%", textAlign: "center" }} htmlFor="password" className="form-label">Password:</label>
        <input type="password" className="form-control" id="password" value={password} onChange={(e) => setPassword(e.target.value)} />
      </div>
      <div className="mb-3">
        <label style={{ width: "100%", textAlign: "center" }} htmlFor="socket" className="form-label">Socket:</label>
        <input type="text" className="form-control" id="socket" value={socket} onChange={(e) => setSocket(e.target.value)} />
      </div>
      <button style={{ width: "100%", textAlign: "center" }} type="submit" className="btn btn-primary" disabled={isSubmitting}>
        {isSubmitting ? 'try to create a connection ...' : 'Submit'}
      </button>
    </form>
  );
};

export default SipForm;
