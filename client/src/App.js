import React from 'react';
import SipForm from './SipForm';

const App = () => {
  const handleSubmit = (config) => {
    // Use the configuration values here to connect to the SIP server
    console.log(config);
  };

  return (
    <div style={{height: "100vh", display: "flex" , alignItems: "center" , justifyContent: "center"}}>
      <SipForm onSubmit={handleSubmit} />
    </div>
  );
};

export default App;
