const express = require('express');
const path = require('path');
const JsSIP = require('jssip');

const app = express();
const port = process.env.PORT || 4000;

// Middleware to parse JSON data
app.use(express.json());

app.use(express.static("client/build"));
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'client/build/index.html'), function(err) {
    if (err) {
      res.status(500).send(err)
    }
  })
})

// Route to handle the SIP configuration data
app.post('/api', async (req, res) => {
  const { uri, password, socket } = req.body;

  // Configuration du client SIP
  const configuration = {
    uri: uri,
    password: password,
    sockets: [new JsSIP.WebSocketInterface(socket)],
  };

  try {
    // Création du client SIP
    const ua = new JsSIP.UA(configuration);

    // Événement lorsque le client SIP est connecté
    ua.on('connected', () => {
      console.log('Connected to SIP server');
      res.send('SIP connection successful');
    });

    // Événement lorsque le client SIP échoue à se connecter
    ua.on('disconnected', () => {
      console.log('Failed to connect to SIP server');
      res.status(500).send('Failed to connect to SIP server');
    });

    // Événement lorsque le client SIP reçoit un appel
    ua.on('newRTCSession', (data) => {
      const session = data.session;
      console.log('Incoming call from:', session.remote_identity.display_name);

      // Répondre automatiquement à l'appel
      session.answer({
        mediaConstraints: { audio: true, video: false },
      });
    });

    // Démarrer le client SIP
    await ua.start();
  } catch (error) {
    console.log('Error connecting to SIP server:', error);
    res.status(500).send('Error connecting to SIP server');
  }
});



app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
});
